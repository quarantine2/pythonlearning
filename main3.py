import time
from PyQt5.QtWidgets import (
  QApplication as QApp,
  QWidget as QWidg,
  QVBoxLayout as QVbox,
  QLabel as QLab,
  QPushButton as QPush,
)

app = QApp([])

myWidget = QWidg()
myWidget.show()

vBox = QVbox()
myWidget.setLayout(vBox)

def myFunction() :
  print('En cours d\'extinction')
  time.sleep(1)
  app.closeAllWindows()

loveLabel = QLab('J\'adore les hiéroglyphes!')
button = QPush('Quitter')
button.clicked.connect(myFunction)

numbers = [1,2,3,4,5,6]

vBox.addWidget(loveLabel)

for number in numbers :
  time.sleep(1)
  label = QLab(str(number))
  vBox.addWidget(label)

vBox.addWidget(button)

app.exec_()
