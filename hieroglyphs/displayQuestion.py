from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import (
  QVBoxLayout as QVbox,
  QLabel as QLab,
  QPushButton as QPush,
  QLineEdit as QLine,
)
from getRandomPair import getRandomPair

def displayQuestion(pair, vBox):
  question = pair['question']
  answer = pair['response']

  questionLabel = QLab('Traduisez le mot : \" '+ question + ' \"')

  successLabel = QLab('Bravo!')

  defeatLabel = QLab('FAUX! La bonne réponse était: '+ answer)

  def reset():
    for index in reversed(range(vBox.count())):
      vBox.itemAt(index).widget().setParent(None)
    displayQuestion(getRandomPair(), vBox)

  timer = QTimer()
  timer.timeout.connect(reset)
  timer.setSingleShot(True)

  def commit() :
    if answerField.text() == answer :
      vBox.addWidget(successLabel)
      timer.start(2000)

    else:
      vBox.addWidget(defeatLabel)
      timer.start(4000)


  vBox.addWidget(questionLabel)

  answerField = QLine()
  vBox.addWidget(answerField)
  answerField.setFocus()

  commitButton = QPush('OK')
  answerField.returnPressed.connect(commitButton.click)
  commitButton.clicked.connect(commit)
  vBox.addWidget(commitButton)
