import random
from dico import dico

keys = list(dico.keys())

def getPairByIndex(index) :
  randomQuestion = keys[index]
  correspondingAnswer = dico[randomQuestion]
  return {'question': randomQuestion, 'response': correspondingAnswer}

def getRandomPair() :
  randomNumber = random.randint(0, len(keys)-1)
  return getPairByIndex(randomNumber)
