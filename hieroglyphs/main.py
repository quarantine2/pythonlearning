from PyQt5.QtWidgets import (
  QApplication as QApp,
  QWidget as QWidg,
  QVBoxLayout as QVbox,
)
from getRandomPair import getRandomPair
from displayQuestion import displayQuestion

randomPair = getRandomPair()

app = QApp([])

myWidget = QWidg()
myWidget.show()

vBox = QVbox()
myWidget.setLayout(vBox)

displayQuestion(randomPair, vBox)

app.exec_()
