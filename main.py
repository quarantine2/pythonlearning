# Déclaration de ma variable
chevalType = 'balsamique'

# Posage de la grande question
print('Comment est mon cheval ?')

# Réponse à la grande question
print('Mon cheval est', chevalType)

# Et la réponse à tout est?
print('Mais j\'aurais pu répondre 42, comme d\'hab')
