import random
from PyQt5.QtWidgets import (
  QApplication as QApp,
  QWidget as QWidg,
  QVBoxLayout as QVbox,
  QLabel as QLab,
  QPushButton as QPush,
  QLineEdit as QLine,
)
from dico import dico

keys = list(dico.keys())
randomNumber = random.randint(0, len(keys)-1)

randomQuestion = keys[randomNumber]
correspondingAnswer = dico[randomQuestion]

app = QApp([])

myWidget = QWidg()
myWidget.show()

vBox = QVbox()
myWidget.setLayout(vBox)

questionLabel = QLab(randomQuestion)

successLabel = QLab('Bravo!')

defeatLabel = QLab('FAUX! La bonne réponse était: '+ correspondingAnswer)

vBox.addWidget(questionLabel)

answerField = QLine()
vBox.addWidget(answerField)

def commit() :
  if answerField.text() == correspondingAnswer :
    vBox.addWidget(successLabel)

  else:
    vBox.addWidget(defeatLabel)


commitButton = QPush('OK')
commitButton.clicked.connect(commit)
vBox.addWidget(commitButton)

app.exec_()
