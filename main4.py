
from PyQt5.QtWidgets import (
  QApplication as QApp,
  QWidget as QWidg,
  QVBoxLayout as QVbox,
  QLabel as QLab,
  QPushButton as QPush,
  QLineEdit as QLine,
)

app = QApp([])

myWidget = QWidg()
myWidget.show()

vBox = QVbox()
myWidget.setLayout(vBox)

questionLabel = QLab('4 + 4 = ')

successLabel = QLab('Bravo!')

defeatLabel = QLab('DTC FDP')

vBox.addWidget(questionLabel)

answerField = QLine()
vBox.addWidget(answerField)

def commit() :
  if answerField.text() == '8' :
    vBox.addWidget(successLabel)

  else:
    vBox.addWidget(defeatLabel)

  # else:
  #   question = input('Votre prénom est-il Benjamin? ')

  #   if question == 'non' :
  #     print('Alors ne l\'imite pas...')

  #   else:
  #     print('Tu es un idiot.')

commitButton = QPush('OK')
commitButton.clicked.connect(commit)
vBox.addWidget(commitButton)

app.exec_()
